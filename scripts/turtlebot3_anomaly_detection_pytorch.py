#!/usr/bin/env python3

import rospy
import importlib
import numpy as np
import json
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Imu
import argparse

import torch
import torch.optim as optim
import torch.nn as nn
from collections import deque
from scipy.stats import multivariate_normal

import time

def decode_seq_data(X, seq2seq):
    rospy.logdebug("X.shape={}".format(X.shape))
    with torch.no_grad():
        #seq2seq.to(seq2seq.device)
        X = torch.from_numpy(X).float().to(seq2seq.device)
        decoded_seq = seq2seq(X).detach().cpu().numpy()
    rospy.logdebug("decoded_seq.shape={}".format(decoded_seq.shape))
    return decoded_seq


class AnomalyDetection():
    def __init__(self, args):
        self.args = args
        #self.cmd_vel_sub = rospy.Subscriber('cmd_vel', Twist, self.get_cmd,
        #    queue_size = 10)
        self.odom_sub = rospy.Subscriber('odom', Odometry, self.get_odom,
            queue_size = 10)
        rospy.loginfo("Logging cmd_vel, imu, and odometry INIT")
        self.recv_odom = deque()
        self.recv_cmd = deque()
        self.curr_seq = None

        self.load_detection_model()
        self.data_collection()

    def load_detection_model(self):
        if self.args.attention:
            cfg_path = 'config.turtlebot3_config_pytorch_attention'
        else:
            cfg_path = 'config.turtlebot3_config_pytorch'
        self.cfg = importlib.import_module(cfg_path).config

        rospy.loginfo(self.cfg)
        # PART 1: Prepare data
        self.data = importlib.import_module('turtlebot3_data')
        self.input_scaler, self.output_scaler, input_data = self.data.get_data(
                                                                self.cfg)
        #self.test_input = input_data[self.cfg['look_back']]
        rospy.loginfo("Loaded dataset")

        # PART 2: load the trained model
        model_dir = "models/{}/{}{}".format(self.cfg['scenario'],
            self.args.model, '_attention' if self.args.attention else '')
        if self.args.model == 'gru_seq2seq':
            import models.gru_seq2seq_pytorch as seq2seq_pytorch
        else:
            import models.lstm_seq2seq_pytorch as seq2seq_pytorch
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        if self.args.attention:
            self.seq2seq = seq2seq_pytorch.AttentionSeq2Seq(self.cfg, self.device)
        else:
            self.seq2seq = seq2seq_pytorch.Seq2Seq(self.cfg, self.device)

        self.seq2seq.load_state_dict(
            torch.load("{}/{}_seq2seq.pth".format(model_dir, self.args.model),
                       map_location=self.device))
        self.seq2seq.eval() # ensure dropout is ignored
        self.seq2seq.to(self.device)
        rospy.loginfo("Loaded seq2seq mode from file.")

        with open("{}/mu_sigma_threshold.json".format(model_dir), "r") as f:
            params = json.load(f)
        self.mu = np.array(params['mu'])
        self.sigma = np.array(params['sigma'])
        self.threshold = params['threshold']
        self.ready_run_predict = True
        rospy.loginfo("Loaded threshold parameters")

    def prediction(self):
        if self.curr_seq is None:
            return

        if len(self.curr_seq) < self.cfg['look_back']:
            rospy.loginfo("not enough points, curr_seq.shape={}".format(
                self.curr_seq.shape))
            return

        # Estimate/Prediction Anomaly
        self.ready_run_predict = False
        # scale input data:
        start_1 = time.time()
        input_seq = self.input_scaler.transform(self.curr_seq)
        input_seq = self.data.sampling(input_seq,
                        subset=self.cfg['downsampling_subset'])
        input_seq = np.expand_dims(input_seq, axis=0)
        start_2 = time.time()
        pred_seq = decode_seq_data(input_seq, self.seq2seq)
        start_3 = time.time()

        recon_err = abs(pred_seq - input_seq)
        recon_err = np.mean(recon_err, axis=1)
        logPD_err = multivariate_normal.logpdf(recon_err, self.mu, self.sigma)
        pred = (logPD_err < self.threshold) * 1.
        start_4 = time.time()
        rospy.loginfo("Duration decoding={}, recon_err={}".format(
            start_3 - start_2, recon_err))
        rospy.loginfo("PRED={}, logPD={}, threshold={}".format(
            pred, logPD_err, self.threshold))
        self.ready_run_predict = True


    def get_cmd(self, recv_cmd):
        """
        rosmsg show geometry_msgs/Twist
            geometry_msgs/Vector3 linear
              float64 x, float64 y, float64 z
            geometry_msgs/Vector3 angular
              float64 x, float64 y, float64 z
        """
        rospy.logdebug("Get cmd_vel")
        now =  rospy.get_rostime()
        self.recv_cmd.append(recv_cmd)

    def get_odom(self, recv_odom):
        """ rosmsg info nav_msgs/Odometry
            std_msgs/Header header
              uint32 seq
              time stamp
              [string frame_id]
            [string child_frame_id]
            geometry_msgs/PoseWithCovariance pose
              geometry_msgs/Pose pose
                geometry_msgs/Point position
                  float64 x, float64 y, float64 z
                geometry_msgs/Quaternion orientation
                  float64 x, float64 y, float64 z, float64 w
              [float64[36] covariance]
            geometry_msgs/TwistWithCovariance twist
              geometry_msgs/Twist twist
                geometry_msgs/Vector3 linear
                  float64 x, float64 y, float64 z
                geometry_msgs/Vector3 angular
                  float64 x, float64 y, float64 z
              [float64[36] covariance]
        """
        rospy.logdebug("Get odom")
        self.recv_odom.append(recv_odom)

        # comment out based on config file.
        new_input = np.array([[
            recv_odom.pose.pose.position.x, recv_odom.pose.pose.position.y
            #,new_odom.pose.pose.position.z, new_odom.pose.pose.orientation.w
            #,new_odom.twist.twist.linear.x, new_odom.twist.twist.angular.z,
            ]])

        if self.curr_seq is None:
            self.curr_seq = new_input
        else:
            if len(self.curr_seq) < self.cfg['look_back']:
                self.curr_seq = np.concatenate((self.curr_seq, new_input),
                                    axis=0)
            else:
                self.curr_seq = np.concatenate((self.curr_seq[1:,:], new_input),
                                    axis=0)

    def data_collection(self):
        rate = rospy.Rate(15)
        while not rospy.is_shutdown():
            if self.ready_run_predict:
                self.prediction()
            rate.sleep()



def main(args):
    LOG_LVL = rospy.INFO
    if args.verbose:
        LOG_LVL = rospy.DEBUG
    rospy.init_node('turtlebot3_data_collection', log_level=LOG_LVL)
    rospy.loginfo("Start turtlebot3_data_collection_node")

    try:
        anomalyDetection = AnomalyDetection(args)
    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--model',
        type=str,
        help='Model name: lstm_seq2seq, and gru_seq2seq (default).',
        default='gru_seq2seq')
    parser.add_argument('--attention',
        action='store_true',
        help='Enable to attention decoder mode.')
    parser.add_argument('--verbose',
        action='store_true',
        help='Enable log debug level.')
    args = parser.parse_args()
    main(args)
