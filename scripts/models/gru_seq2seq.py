from __future__ import division

import tensorflow as tf
from keras.layers import Input, Dense, Dropout, Bidirectional, Concatenate
from keras.models import Model
from keras.optimizers import RMSprop, Adam, SGD
from keras.regularizers import l2
import logging

def build_models(use_gpu, cfg):
    """ Construct the model with CPU/GPU GRU, and load trained weights to
    this model. myGRU could be CuDNNGRU or GRU
    """
    if use_gpu:
        if tf.config.experimental.list_physical_devices('GPU'):
            # Only use CuDNNGRU for training Cloud model.
            from keras.layers import CuDNNGRU as myGRU
        else:
            from keras.layers import GRU as myGRU
    else:
        from keras.layers import GRU as myGRU

    encoder_layers = cfg['encoder_layers']
    decoder_layers = cfg['decoder_layers']
    n_feature_in = len(cfg['feature_in'])
    n_feature_out = len(cfg['feature_out'])
    loss = cfg['loss']
    learning_rate = cfg['learning_rate']
    if cfg['optimizer']=='Adam':
        optimizer = Adam(learning_rate)
    elif cfg['optimizer'] == 'RMSProp':
        optimizer = RMSprop(learning_rate)
    else:
        optimizer = SGD(learning_rate)

    droprate = cfg['droprate']

    #### ENCODER:
    encoder_inputs = Input(batch_shape=(None, None, n_feature_in))
    logging.info("encoder_inputs={}".format(encoder_inputs.get_shape()))

    max_len = len(cfg['encoder_layers'])
    # single layer:
    return_sequences = True
    if cfg['bidirection']:
        e_outputs, ef_h, eb_h = Bidirectional(myGRU(
                            encoder_layers[0],
                            kernel_regularizer=l2(1e-3),
                            return_sequences=return_sequences,
                            return_state=True))(encoder_inputs)
        state_h = Concatenate()([ef_h, eb_h])
    else: # UNIDIRECTION
        e_outputs, state_h = myGRU(encoder_layers[0],
                            kernel_regularizer=l2(1e-4),
                            return_sequences=return_sequences,
                            return_state=True)(encoder_inputs)

    encoder_states = [state_h]
    e_outputs = Dropout(droprate)(e_outputs)
    e_inputs = e_outputs

    if max_len > 1: # multiple layers
        if cfg['bidirection']:
            for i, layer_dim in enumerate(encoder_layers[1:]):
                e_outputs, ef_h, eb_h =\
                    Bidirectional(myGRU(layer_dim,
                            kernel_regularizer=l2(1e-4),
                            return_sequences=return_sequences,
                            return_state=True))(e_inputs)
                state_h = Concatenate()([ef_h, eb_h])
                encoder_states.append(state_h)
                if i < max_len - 1:
                    e_outputs = Dropout(droprate)(e_outputs)
                    e_inputs = e_outputs
        else: # unidirection
            # from the second layer we don't need to provide batch_input_shape
            for i, layer_dim in enumerate(encoder_layers[1:]):
                e_outputs, state_h = myGRU(layer_dim,
                            kernel_regularizer=l2(1e-4),
                            return_sequences=return_sequences,
                            return_state=True)(e_inputs)
                encoder_states.append(state_h)
                if i < max_len - 1:
                    e_outputs = Dropout(droprate)(e_outputs)
                    e_inputs = e_outputs

    logging.info("encoder_states={}".format(encoder_states))

    ### DECODER: set up the decoder, using `encoder_states` as initial state.
    decoder_inputs = Input(shape=(None, n_feature_out))
    # We set up our decoder to return full output sequences,
    # and to return internal states as well. We don't use the
    # return states in the training model, but we will use them in inference.
    initial_states = encoder_states
    d_inputs = decoder_inputs
    decoder_states_inputs = [] # for define Model later
    decoder_grus = []
    for i, layer_dim in enumerate(decoder_layers):
        if cfg['bidirection']:
            factor = 2
        else:
            factor = 1
        decoder_gru = myGRU(layer_dim * factor,
                            kernel_regularizer=l2(1e-4),
                            return_sequences=True,
                            return_state=True)
        d_outputs, dhi = decoder_gru(d_inputs,
                            initial_state=initial_states[i:i+1])
        logging.info("i={}, dhi={}".format(i, dhi))
        # input of the higher layer is the output of the previously lower layer

        d_outputs = Dropout(droprate)(d_outputs)
        d_inputs = d_outputs

        # For definition Model later
        d_input_h = Input(shape=(layer_dim*factor, ))
        decoder_states_inputs.append(d_input_h)
        decoder_grus.append(decoder_gru)

    decoder_dense = Dense(n_feature_out, activation='linear')
    decoder_outputs = decoder_dense(d_outputs)

    # SAVE Model: Define the model that will be saved
    # `encoder_input_data` & `decoder_input_data` into `decoder_target_data`
    model = Model(inputs=[encoder_inputs, decoder_inputs],
                  outputs=decoder_outputs)
    # Encoder Model
    encoder_model = Model(inputs=encoder_inputs, outputs=encoder_states)
    # Decoder Model
    de_inputs = decoder_inputs
    de_states_outputs = []
    for i, layer_dim in enumerate(decoder_layers):
        decoder_gru = decoder_grus[i]
        de_states_inputs = decoder_states_inputs[i:i+1]
        de_outputs, state_h = decoder_gru(de_inputs,
                            initial_state=de_states_inputs)
        de_states_outputs += [state_h]
        de_inputs = de_outputs

    de_outputs = decoder_dense(de_outputs)
    decoder_model = Model(inputs=[decoder_inputs] + decoder_states_inputs,
                            outputs=[de_outputs] + de_states_outputs)
    # Run training
    model.compile(optimizer = optimizer,
                  loss = loss,
                  metrics = ['accuracy'])
    logging.info(model.summary())

    return model, encoder_model, decoder_model
