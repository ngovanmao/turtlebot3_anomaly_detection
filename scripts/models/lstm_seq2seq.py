from __future__ import division

import tensorflow as tf
from keras.layers import Input, Dense, Dropout, Bidirectional, Concatenate
from keras.models import Model
from keras.optimizers import RMSprop, Adam, SGD
from keras.regularizers import l2
import logging

def build_models(use_gpu, cfg):
    """ Construct the model with CPU/GPU LSTM, and load trained weights
     to this model. myLSTM could be CuDNNLSTM or LSTM
    """
    if use_gpu:
        if tf.config.experimental.list_physical_devices('GPU'):
            # Only use CuDNNLSTM for training Cloud model.
            from keras.layers import CuDNNLSTM as myLSTM
        else:
            from keras.layers import LSTM as myLSTM
    else:
        from keras.layers import LSTM as myLSTM

    encoder_layers = cfg['encoder_layers']
    decoder_layers = cfg['decoder_layers']
    n_feature_in = len(cfg['feature_in'])
    n_feature_out = len(cfg['feature_out'])
    loss = cfg['loss']
    learning_rate = cfg['learning_rate']
    droprate = cfg['droprate']
    if cfg['optimizer']=='Adam':
        optimizer = Adam(learning_rate)
    elif cfg['optimizer'] == 'RMSProp':
        optimizer = RMSprop(learning_rate)
    else:
        optimizer = SGD(learning_rate)


    #### ENCODER:
    encoder_inputs = Input(batch_shape=(None, None, n_feature_in))
    logging.info("encoder_inputs={}".format(encoder_inputs.get_shape()))

    max_len = len(cfg['encoder_layers'])
    # single layer:
    return_sequences = True
    if cfg['bidirection']:
        e_outputs, ef_h, ef_c, eb_h, eb_c = Bidirectional(myLSTM(
                            encoder_layers[0],
                            kernel_regularizer=l2(1e-3),
                            return_sequences=return_sequences,
                            return_state=True))(encoder_inputs)
        state_h = Concatenate()([ef_h, eb_h])
        state_c = Concatenate()([ef_c, eb_c])
    else: # UNIDIRECTION
        e_outputs, state_h, state_c = myLSTM(encoder_layers[0],
                            kernel_regularizer=l2(1e-4),
                            return_sequences=return_sequences,
                            return_state=True)(encoder_inputs)

    encoder_states = [state_h, state_c]
    e_outputs = Dropout(droprate)(e_outputs)
    e_inputs = e_outputs

    if max_len > 1: # multiple layers
        if cfg['bidirection']:
            for i, layer_dim in enumerate(encoder_layers[1:]):
                e_outputs, ef_h, ef_c, eb_h, eb_c =\
                    Bidirectional(myLSTM(layer_dim,
                            kernel_regularizer=l2(1e-4),
                            return_sequences=return_sequences,
                            return_state=True))(e_inputs)
                state_h = Concatenate()([ef_h, eb_h])
                state_c = Concatenate()([ef_c, eb_c])
                encoder_states.append(state_h)
                encoder_states.append(state_c)
                if i < max_len - 1:
                    e_outputs = Dropout(droprate)(e_outputs)
                    e_inputs = e_outputs
        else: # unidirection
            # from the second layer we don't need to provide batch_input_shape
            for i, layer_dim in enumerate(encoder_layers[1:]):
                e_outputs, state_h, state_c = myLSTM(layer_dim,
                            kernel_regularizer=l2(1e-4),
                            return_sequences=return_sequences,
                            return_state=True)(e_inputs)
                encoder_states.append(state_h)
                encoder_states.append(state_c)
                if i < max_len - 1:
                    e_outputs = Dropout(droprate)(e_outputs)
                    e_inputs = e_outputs

    logging.info("encoder_states={}".format(encoder_states))

    ### DECODER: set up the decoder, using `encoder_states` as initial state.
    decoder_inputs = Input(shape=(None, n_feature_out))
    # We set up our decoder to return full output sequences,
    # and to return internal states as well. We don't use the
    # return states in the training model, but we will use them in inference.
    initial_states = encoder_states
    d_inputs = decoder_inputs
    decoder_states_inputs = [] # for define Model later
    decoder_lstms = []
    for i, layer_dim in enumerate(decoder_layers):
        if cfg['bidirection']:
            factor = 2
        else:
            factor = 1
        decoder_lstm = myLSTM(layer_dim * factor,
                            kernel_regularizer=l2(1e-4),
                            return_sequences=True,
                            return_state=True)
        d_outputs, dhi, dci = decoder_lstm(d_inputs,
                            initial_state=initial_states[i*2:i*2+2])
        logging.info("i={}, dhi={}, dci={}".format(i, dhi, dci))
        # input of the higher layer is the output of the previously lower layer

        d_outputs = Dropout(droprate)(d_outputs)
        d_inputs = d_outputs

        # For definition Model later
        d_input_h = Input(shape=(layer_dim*factor, ))
        d_input_c = Input(shape=(layer_dim*factor, ))
        decoder_states_inputs.append(d_input_h)
        decoder_states_inputs.append(d_input_c)
        decoder_lstms.append(decoder_lstm)

    decoder_dense = Dense(n_feature_out, activation='linear')
    decoder_outputs = decoder_dense(d_outputs)

    # SAVE Model: Define the model that will be saved
    # `encoder_input_data` & `decoder_input_data` into `decoder_target_data`
    model = Model(inputs=[encoder_inputs, decoder_inputs],
                  outputs=decoder_outputs)
    # Encoder Model
    encoder_model = Model(inputs=encoder_inputs,
                          outputs=encoder_states)
    # Decoder Model
    de_inputs = decoder_inputs
    de_states_outputs = []
    for i, layer_dim in enumerate(decoder_layers):
        decoder_lstm = decoder_lstms[i]
        de_states_inputs = decoder_states_inputs[i*2:i*2+2]
        de_outputs, state_h, state_c = decoder_lstm(de_inputs,
                            initial_state=de_states_inputs)
        de_states_outputs += [state_h, state_c]
        de_inputs = de_outputs

    de_outputs = decoder_dense(de_outputs)
    decoder_model = Model(inputs=[decoder_inputs] + decoder_states_inputs,
                           outputs=[de_outputs] + de_states_outputs)
    # Run training
    model.compile(optimizer = optimizer,
                  loss = loss,
                  metrics = ['accuracy'])
    logging.info(model.summary())

    return model, encoder_model, decoder_model
