from __future__ import division

import torch
import torch.nn as nn
import torch.nn.functional as F

import logging

class EncoderRNN(nn.Module):
    def __init__(self, cfg):
        super(EncoderRNN, self).__init__()
        self.input_size = len(cfg['feature_in'])
        self.hidden_size = cfg['hidden_size']
        self.num_layers = cfg['num_layers']
        self.bidirectional = cfg['bidirection_encoder']
        self.num_dir = 2 if self.bidirectional else 1
        self.droprate = cfg['droprate']

        self.gru = nn.GRU(input_size = self.input_size,
                          hidden_size = self.hidden_size,
                          batch_first = True,
                          dropout = self.droprate,
                          num_layers = self.num_layers,
                          bidirectional = self.bidirectional)

    def forward(self, input, hidden):
        """
        Args:
            - input: [batch_size, seq_len, feature_in]
            - hidden: [num_layers*num_dir, batch_size, hidden_size]
        Outputs:
            - output: [batch_size, seq_len, hidden_size*num_dir]
            - hidden: [num_layers*num_dir, batch_size, hidden_size]
        """
        output, hidden = self.gru(input, hidden)
        return output, hidden


class DecoderRNN(nn.Module):
    def __init__(self, cfg):
        super(DecoderRNN, self).__init__()
        self.input_size = len(cfg['feature_out'])
        self.bidirectional = cfg['bidirection_decoder']
        self.hidden_size = cfg['hidden_size']
        # If biGRU encoder and uni-GRU decoder and concatenate encoding states
        if cfg['bidirection_encoder'] and not cfg['bidirection_decoder']:
            if cfg['combine_encoder_states'] == 'concat':
                self.hidden_size = cfg['hidden_size'] * 2
        self.num_layers = cfg['num_layers']
        self.output_size = len(cfg['feature_out'])
        self.num_dir = 2 if self.bidirectional else 1
        self.droprate = cfg['droprate']

        self.gru = nn.GRU(input_size = self.input_size,
                          hidden_size = self.hidden_size,
                          batch_first = True,
                          num_layers = self.num_layers,
                          dropout = self.droprate,
                          bidirectional=self.bidirectional,
                          )
        self.leaky_relu = nn.LeakyReLU()
        self.out = nn.Linear(self.hidden_size*self.num_dir, self.output_size)

    def forward(self, input, hidden):
        """
        Here we decode one time-step at a time, i.e., seq_len = 1.
        Args:
           - input: [batch_size, 1, feature_in], e.g., [64, 1, 2]
           - hidden: [num_layers*num_dir, batch_size, hidden_size], e.g., [1, 64, 300]
        Intermediate output=[batch_size, 1, hidden_size*num_dir]
        Output:
           - output: [batch_size, 1, feature_out], e.g., [64, 1, 2]
           - hidden: [num_layers*num_dir, batch_size, hidden_size]
        """
        #logging.debug("hidden={}".format(hidden.shape, cell.shape))
        output, hidden = self.gru(input, hidden)
        output = self.leaky_relu(output)
        output = self.out(output)
        return output, hidden

class Seq2Seq(nn.Module):
    def __init__(self, cfg, device=torch.device('cpu')):
        super(Seq2Seq, self).__init__()
        self.encoder = EncoderRNN(cfg)
        self.decoder = DecoderRNN(cfg)
        self.device = device
        self.cfg = cfg
        window_size = int(cfg['look_back']/cfg['downsampling_subset'])
        if cfg['look_ahead_only']:
            self.out_seq_len = cfg['look_ahead']
        else:
            self.out_seq_len = window_size # full window

    def forward(self, e_input, target=None, teacher_forcing=1.0, train=False):
        batch_size = e_input.size(0) #0-batch_size, 1-seq_len, 2-feature_in
        # ENCODE
        # we don't need to loop through seq_len to generate outputs,
        # pytorch can do it with properly input (batch_size,seq_len,feature_in)
        e_hidden = torch.zeros(self.encoder.num_layers*self.encoder.num_dir,
            batch_size, self.encoder.hidden_size, device=self.device)
        e_output, e_hidden = self.encoder(e_input, e_hidden)

        # DECODE
        d_hidden = e_hidden
        # For unidirectional decoder
        if self.encoder.bidirectional and not self.decoder.bidirectional:
            # e_hidden=[num_layers*num_dir, batch_size, hidden_size]
            # convert to d_hidden=[num_layers, batch_size,
            #            hidden_size(forward)+hidden_size(backward)]
            if self.cfg['combine_encoder_states'] == 'concat':
                d_hidden = torch.cat((e_hidden[:1,:,:], e_hidden[1:,:,:]), dim=2)
            else:
                d_hidden = e_hidden[:1,:,:] + e_hidden[1:,:,:]

        d_output = None
        # START token is just a zeros tensor:
        d_in = torch.zeros(batch_size, 1, self.decoder.input_size,
            device=self.device)
        if train: # TRAINING mode
            for di in range(self.out_seq_len):
                d_out, d_hidden = self.decoder(d_in, d_hidden)
                if torch.rand(1) < teacher_forcing:
                    # teacher forces to use output as input
                    d_in = target[:,di:di+1,:]
                else:
                    d_in = d_out
                if d_output is None:
                    d_output = d_out
                else:
                    d_output = torch.cat((d_output, d_out), dim=1)
        else: # INFERENCE mode
            for di in range(self.out_seq_len):
                d_out, d_hidden = self.decoder(d_in, d_hidden)
                d_in = d_out
                if d_output is None:
                    d_output = d_out
                else:
                    d_output = torch.cat((d_output, d_out), dim=1)

        return d_output


class AttentionDecoderRNN(nn.Module):
    def __init__(self, cfg):
        super(AttentionDecoderRNN, self).__init__()

        self.input_size = len(cfg['feature_out'])
        self.num_layers = cfg['num_layers']
        self.output_size = len(cfg['feature_out'])
        self.bidirectional = cfg['bidirection_decoder']
        assert not self.bidirectional, 'Wrong configuration'
        #self.num_dir = 2 if self.bidirectional else 1
        self.hidden_size = cfg['hidden_size']
        if cfg['bidirection_encoder'] and not cfg['bidirection_decoder']:
            if cfg['combine_encoder_states'] == 'concat':
                self.hidden_size = cfg['hidden_size'] * 2
        self.droprate = cfg['droprate']
        window_size = int(cfg['look_back']/cfg['downsampling_subset'])
        self.max_len = window_size

        self.attn = nn.Linear(self.hidden_size+self.input_size,
                              self.max_len)
        self.attn_combine = nn.Linear(self.hidden_size+self.input_size,
                                      self.hidden_size)
        self.dropout = nn.Dropout(self.droprate)
        self.gru = nn.GRU(input_size = self.hidden_size,
                          hidden_size = self.hidden_size,
                          num_layers = self.num_layers,
                          batch_first = True,
                          dropout = self.droprate,
                          bidirectional=self.bidirectional)
        self.leaky_relu = nn.LeakyReLU()
        self.out = nn.Linear(self.hidden_size, self.output_size)

    def forward(self, x, hidden, e_outputs):
        """
        We follow the tutorial:
        https://pytorch.org/tutorials/intermediate/seq2seq_translation_tutorial.html

        Input:
          x shape=[batch_size, seq_len=1, input_size(=feature_out)]
          hidden shape=[num_layers*num_dir, batch_size, hidden_size]
          e_outputs shape=[batch_size, seq_len, hidden_size*num_dir]
        Note: here we use max_len=seq_len

        Output:
          output shape=[batch_size, 1, feature_out]
          context_vector shape=[batch_size, max_len, 1]
          attention_weights shape=[batch_size, hidden size]
        """
        # change hidden to shape=(batch_size, 1, hidden_size)
        hidden_t = torch.transpose(hidden, 0, 1)

        # combine hidden and x,
        # combine shape=(batch_size, 1, hidden_size+input_size)
        combine = torch.cat((x, hidden_t), dim=2)

        # score shape = (batch_size, 1, max_len)
        score = self.attn(combine)

        # attn_weights shape=(batch_size, 1, max_len)
        attn_weights = F.softmax(score, dim=1)

        # context_vector shape=(batch_size, 1, hidden_size)
        # batch of maxtrix [1, max_len]x[(max_len=)seq_len, hidden_size]
        context_vector = torch.bmm(attn_weights, e_outputs)

        # output shape after cat (batch_size, 1, hidden_size+feature_out)
        output = torch.cat((x, context_vector), dim=2)

        # attention vector or attention combined
        # shape=(batch_size, 1, hidden_size)
        output = self.attn_combine(output)
        output = F.relu(output)

        # output shape=[batch_size, seq_len=1, hidden_size*num_dir]
        # hidden shape=[num_layers*num_dir, batch_size, hidden_size]
        output, hidden = self.gru(output, hidden)
        #logging.debug('hidden={}'.format(hidden.shape))
        #logging.debug('output={}'.format(output.shape))

        # output shape=[batch_size, 1, feature_out]
        output = self.out(output)

        return output, hidden, attn_weights

class AttentionSeq2Seq(nn.Module):
    def __init__(self, cfg, device=torch.device('cpu')):
        super(AttentionSeq2Seq, self).__init__()
        self.encoder = EncoderRNN(cfg)
        self.decoder = AttentionDecoderRNN(cfg)
        self.device = device
        self.cfg = cfg
        window_size = int(cfg['look_back']/cfg['downsampling_subset'])
        if cfg['look_ahead_only']:
            self.out_seq_len = cfg['look_ahead']
        else:
            self.out_seq_len = window_size # full window

    def forward(self, e_input, target=None, teacher_forcing=1.0, train=False):
        batch_size = e_input.size(0) #0-batch_size, 1-seq_len, 2-feature_in
        # ENCODE
        # we don't need to loop through seq_len to generate outputs,
        # pytorch can do it with properly input (batch_size,seq_len,feature_in)
        e_hidden = torch.zeros(self.encoder.num_layers*self.encoder.num_dir,
            batch_size, self.encoder.hidden_size, device=self.device)
        e_output, e_hidden = self.encoder(e_input, e_hidden)
        # DECODE
        d_hidden = e_hidden

        # For unidirectional decoder
        if self.encoder.bidirectional and not self.decoder.bidirectional:
            # e_hidden=[num_layers*num_dir, batch_size, hidden_size]
            # convert to d_hidden=[num_layers, batch_size,
            #            hidden_size(forward)+hidden_size(backward)]
            if self.cfg['combine_encoder_states'] == 'concat':
                d_hidden = torch.cat((e_hidden[:1,:,:], e_hidden[1:,:,:]), dim=2)
            else: # combine as a sum of two directions
                # d_hidden shape=(1, batch_size, hidden_size)
                d_hidden = e_hidden[:1,:,:] + e_hidden[1:,:,:]
                # e_output shape=[batch_size, seq_len, hidden_size*num_dir]
                e_output = e_output[:, :, :self.encoder.hidden_size] +\
                           e_output[:, :, self.encoder.hidden_size:]

        d_output = None
        # START token is just a zeros tensor:
        d_in = torch.zeros(batch_size, 1, self.decoder.input_size,
            device=self.device)
        if train: # TRAINING mode
            for di in range(self.out_seq_len):
                d_out, d_hidden, _ = self.decoder(d_in, d_hidden, e_output)
                if torch.rand(1) < teacher_forcing:
                    # teacher forces to use output as input
                    d_in = target[:,di:di+1,:]
                else:
                    d_in = d_out
                if d_output is None:
                    d_output = d_out
                else:
                    d_output = torch.cat((d_output, d_out), dim=1)
        else: # INFERENCE mode
            for di in range(self.out_seq_len):
                d_out, d_hidden, att_weights = self.decoder(d_in, d_hidden,
                                                           e_output)
                d_in = d_out
                if d_output is None:
                    d_output = d_out
                else:
                    d_output = torch.cat((d_output, d_out), dim=1)

        return d_output
