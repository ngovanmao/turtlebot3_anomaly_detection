config={
    #"data":
    'robot':'turtlebot3',
    'scenario':'triangle', # or square, or zigzag, or house
    'look_back':120,
    'look_ahead':1, # half second look ahead
    'step_window':1,
    'feature_in':['pose_pose_position_x','pose_pose_position_y'
        ,'pose_pose_orientation_z','pose_pose_orientation_w'
        ,'twist_twist_linear_x','twist_twist_angular_z'
        ],
    'feature_out':['pose_pose_position_x','pose_pose_position_y'
        ,'pose_pose_orientation_z','pose_pose_orientation_w'
        ],
    'inversed':False,
    'synthesized_test':True,
    'synthesized_prob':0.005,
    'synthesized_type':'spike', # either spike or burst
    'synthesized_burst_width':10, # 10 sample width
    #--------------------------------------------------------------

    #"model":
    'hidden_size':300,
    'num_layers':1,
    'droprate':0.10, #only apply when num_layers>1
    'bidirection':True,
    # either "lstm_seq2seq" or "gru_seq2seq" use *_tf.py file
    # or "lstm_seq2seq_pytorch", "gru_seq2seq_pytorch" use *_pytorch.py file
    #'rnn':'lstm_seq2seq',
    'rnn':'gru_seq2seq',
    #--------------------------------------------------------------
    # Training:
    'learning_rate':0.0005,
    'droprate': 0.10,
    'loss':'mean_squared_error',
    'optimizer':'Adam',

    #--------------------------------------------------------------

    # inference
    'batch_size_inference':256,
    'load_weights':True,
    'use_gpu':False,
    #--------------------------------------------------------------
}
