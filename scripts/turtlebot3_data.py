import pandas as pd
import time as time
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
import logging
import random

# Apply only for training data,
# then use returned scaler object to scale test data
def standardize(data):
    scaler = StandardScaler()
    logging.debug("shape data={}".format(data.shape))
    data = scaler.fit_transform(data)
    logging.debug("shape transformed data={}".format(data.shape))
    logging.debug("Scaler Mean: {}".format(scaler.mean_))
    logging.debug("Scaler Variance: {}".format(scaler.var_))
    logging.debug("data mean {}, data variance {}".format(
        np.mean(data, axis=0),np.var(data, axis=0)))
    return data, scaler

def normalize(data):
    scaler = MinMaxScaler()
    data = scaler.fit_transform(data)
    return data, scaler

def sampling(data, subset=4, method='mean'):
    """
    data: shape=(num_samples, num_feature)
    output: shape=(ceil(num_samples/subset), num_feature)
    """
    assert subset > 0, 'wrong sampling interval, please choose positive number'
    output = []
    i = 0
    while i < len(data):
        w = data[i:i+subset]
        if method == 'mean':
            w = np.mean(w,axis=0)
        output.append(w)
        i += subset
    return np.array(output)

def prepare_seq2seq_data(input_dataset, output_dataset, look_back, look_ahead,
        step_window):
    #logging.debug("dataset.shape={}".format(dataset.shape))
    #dataset = np.vstack((dataset, dataset[-(look_back+look_ahead):]))
    logging.debug("input_dataset={}, output_dataset={}".format(
        input_dataset.shape, output_dataset.shape))
    data_input, data_target = [], []
    for i in range(0,len(input_dataset) - look_back - look_ahead+1, step_window):
        input_seq = input_dataset[i:(i + look_back)]
        output_seq = output_dataset[(i + look_ahead):(i + look_back + look_ahead)]
        data_input.append(input_seq)
        data_target.append(output_seq)
    data_input = np.array(data_input)
    data_target = np.array(data_target)
    logging.debug("data_input.shape={}, data_target.shape={}".format(
        data_input.shape, data_target.shape))
    return data_input, data_target

def get_raw_data(cfg):
    # Load data from csv
    data_dir = "dataset/{}/normal_{}".format(cfg['robot'], cfg['scenario'])
    start = time.time()
    df = pd.read_csv('{}/odometry.csv'.format(data_dir))
    logging.info("load dataset from csv files, take={}".format(
        time.time()-start))
    in_dataset = df[cfg['feature_in']]
    out_dataset = df[cfg['feature_out']]
    len_data =len(in_dataset) 
    if cfg['synthesized_test']:
        ratio_train_test = 0.7
    else:
        ratio_train_test = 0.85
    train_input_set = in_dataset[:int(ratio_train_test*len_data)]
    train_output_set = out_dataset[:int(ratio_train_test*len_data)]
    train_input_set = sampling(train_input_set, cfg['downsampling_subset'])
    train_output_set = sampling(train_output_set, cfg['downsampling_subset'])
    len_data =len(in_dataset) # two datasets have the same length.

    logging.info("train_input_set={}, train_output_set={}".format(
        train_input_set.shape, train_output_set.shape))

    return train_input_set, train_output_set

def get_data(cfg):
    origin_input, origin_output = get_raw_data(cfg)
    # standardize train set and transform test set
    if cfg['scale'] == 'standardize':
        input_data, input_scaler = standardize(origin_input)
        output_data, output_scaler = standardize(origin_output)
    else:
        input_data, input_scaler = normalize(origin_input)
        output_data, output_scaler = normalize(origin_output)

    return input_scaler, output_scaler, input_data
