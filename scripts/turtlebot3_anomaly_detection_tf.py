#!/usr/bin/env python3

import rospy
import importlib
import numpy as np
import json
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Imu
from keras.models import load_model
from collections import deque
from scipy.stats import multivariate_normal

import time

def decode_seq_data(X, MIN_INPUT, MAX_INPUT, encoder_model, decoder_model, cfg):
    rospy.logdebug("X.shape={}".format(X.shape))
    batch_size = 1
    START = np.full((batch_size, 1, len(cfg['feature_out'])), MIN_INPUT)
    STOP = np.full((batch_size, len(cfg['feature_out'])), MAX_INPUT)

    encoded_states = encoder_model.predict(X, batch_size=batch_size)
    target_seq = START
    # Quick-fix for GRU-Keras with Single layer GRUs:
    if type(encoded_states).__module__ == np.__name__:
        # Single layer, just one numpy array
        decoder_states = [encoded_states]
    else:
        decoder_states = encoded_states
    decoded_seq = None
    stop_condition = False
    while not stop_condition:
        outs = decoder_model.predict([target_seq] + decoder_states)
        output_value = outs[0]
        decoder_states = outs[1:]
        # Exit condition: either hit max length or find stop character.
        #rospy.logdebug("decoded_seq.shape={}".format(decoded_seq.shape))
        if (output_value == STOP).all():
            break # no need to concatenate STOP to output sequence
        if decoded_seq is None:
            decoded_seq = output_value
        else:
            decoded_seq = np.concatenate((decoded_seq, output_value), axis=1)
        if decoded_seq.shape[1] > cfg['look_back'] - 1:
            stop_condition = True
        # Update the target sequence (of length 1).
        target_seq = output_value

    if cfg['inversed']: # reverse again
        decoded_seq = decoded_seq[:,::-1,:]

    rospy.logdebug("decoded_seq.shape={}".format(decoded_seq.shape))
    return decoded_seq


class AnomalyDetection():
    def __init__(self):
        #self.cmd_vel_sub = rospy.Subscriber('cmd_vel', Twist, self.get_cmd,
        #    queue_size = 10)
        self.odom_sub = rospy.Subscriber('odom', Odometry, self.get_odom,
            queue_size = 10)
        rospy.loginfo("Logging cmd_vel, imu, and odometry INIT")
        self.recv_odom = deque()
        self.recv_cmd = deque()

        self.load_detection_model()
        self.data_collection()

    def load_detection_model(self):
        self.cfg = importlib.import_module('turtlebot3_config').config
        rospy.loginfo(self.cfg)
        # PART 1: Prepare data
        data = importlib.import_module('dataset.turtlebot3_data')
        self.input_scaler, self.output_scaler, input_data = data.get_data(self.cfg)
        self.MIN_INPUT = 1.1*np.min(input_data)
        self.MAX_INPUT = 1.1*np.max(input_data)
        self.test_input = input_data[self.cfg['look_back']]
        self.last_seq = None
        rospy.loginfo("Loaded dataset")

        # PART 2: load the trained model
        model_dir = "models/{}".format(self.cfg['rnn'])
        if self.cfg['load_weights']:
            rospy.loginfo("Load model from WEIGHT")
            if self.cfg['rnn'] == 'lstm_seq2seq':
                import models.lstm_seq2seq as base_model
            else:
                import models.gru_seq2seq as base_model

            model, self.encoder_model, self.decoder_model =\
                base_model.build_models(True, self.cfg)
            self.encoder_model.load_weights("{}/{}_encoder.weights".format(
                model_dir, self.cfg['rnn']))
            self.decoder_model.load_weights("{}/{}_decoder.weights".format(
                model_dir, self.cfg['rnn']))
        else:
            rospy.loginfo("Load model from MODEL H5 files")
            self.encoder_model = load_model("{}/{}_encoder.h5".format(
                model_dir, self.cfg['rnn']))
            self.decoder_model = load_model("{}/{}_decoder.h5".format(
                model_dir, self.cfg['rnn']))

        with open("{}/mu_sigma_threshold.json".format(model_dir), "r") as f:
            params = json.load(f)
        self.mu = np.array(params['mu'])
        self.sigma = np.array(params['sigma'])
        self.threshold = params['threshold']
        self.ready_run_predict = True
        rospy.loginfo("Loaded encoder and decoder models")

    def prediction(self):
        try:
            new_odom = self.recv_odom.popleft()
        except IndexError:
            rospy.logerror("Empty recv_odom queue")
            return

        new_input = np.array([
            new_odom.pose.pose.position.x, new_odom.pose.pose.position.y
            #,new_odom.pose.pose.position.z, new_odom.pose.pose.orientation.w
            #,new_odom.twist.twist.linear.x, new_odom.twist.twist.angular.z,
            ])
        if self.last_seq is None:
            self.last_seq = np.array([new_input])

        if len(self.last_seq) != self.cfg['look_back']:
            self.last_seq = np.concatenate((self.last_seq, np.array([new_input])), axis=0)
            rospy.loginfo("not enough points, last_seq.shape={}".format(
                self.last_seq.shape)
            )
        else:
            input_seq = np.concatenate((self.last_seq[1:], np.array([new_input])),
                axis=0)
            self.last_seq = input_seq
            self.ready_run_predict = False
            # scale input data:
            start_1 = time.time()
            s_input_seq = self.input_scaler.transform(input_seq)
            start_2 = time.time()
            s_decoded_seq = decode_seq_data(np.array([s_input_seq]),
                self.MIN_INPUT, self.MAX_INPUT,
                self.encoder_model, self.decoder_model, self.cfg)
            start_3 = time.time()
            decoded_seq = self.output_scaler.inverse_transform(s_decoded_seq[0])
            recon_err = np.abs(decoded_seq - input_seq)
            logPD_err = multivariate_normal.logpdf(recon_err, self.mu, self.sigma)
            pred = (logPD_err < self.threshold) * 1.
            start_4 = time.time()
            rospy.loginfo("Duration input_transform={}, decoding={},"
            " output_transform={}".format(start_2-start_1, start_3 - start_2,
             start_4 - start_3))

            rospy.loginfo("prediction={}".format(pred))
            self.ready_run_predict = True

    def get_cmd(self, recv_cmd):
        """
        rosmsg show geometry_msgs/Twist
            geometry_msgs/Vector3 linear
              float64 x, float64 y, float64 z
            geometry_msgs/Vector3 angular
              float64 x, float64 y, float64 z
        """
        rospy.logdebug("Get cmd_vel")
        now =  rospy.get_rostime()
        self.recv_cmd.append(recv_cmd)


    def get_odom(self, recv_odom):
        """ rosmsg info nav_msgs/Odometry
            std_msgs/Header header
              uint32 seq
              time stamp
              [string frame_id]
            [string child_frame_id]
            geometry_msgs/PoseWithCovariance pose
              geometry_msgs/Pose pose
                geometry_msgs/Point position
                  float64 x, float64 y, float64 z
                geometry_msgs/Quaternion orientation
                  float64 x, float64 y, float64 z, float64 w
              [float64[36] covariance]
            geometry_msgs/TwistWithCovariance twist
              geometry_msgs/Twist twist
                geometry_msgs/Vector3 linear
                  float64 x, float64 y, float64 z
                geometry_msgs/Vector3 angular
                  float64 x, float64 y, float64 z
              [float64[36] covariance]
        """
        rospy.logdebug("Get odom")
        self.recv_odom.append(recv_odom)


    def data_collection(self):
        rate = rospy.Rate(15)
        while not rospy.is_shutdown():
            if self.ready_run_predict:
                self.prediction()
            rate.sleep()



def main():
    rospy.init_node('turtlebot3_data_collection')
    rospy.loginfo("Start turtlebot3_data_collection_node")

    try:
        anomalyDetection = AnomalyDetection()
    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    main()
