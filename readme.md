We create soft-links to the source of `robot_anomaly_detection` repository to reuse the updated implementations.

```
scripts/config -> /home/xmaongo/robot_anomaly_detection/config
scripts/dataset -> /home/xmaongo/robot_anomaly_detection/dataset
scripts/models/pentagon -> /home/xmaongo/robot_anomaly_detection/trained_model/turtlebot3/pentagon/
```
